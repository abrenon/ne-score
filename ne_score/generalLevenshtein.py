from functools import reduce

class Update:
    def __init__(self, initial, final):
        self.initial = initial
        self.final = final
    def __str__(self):
        return str(self.initial) + " -> " + str(self.final)

class Edition:
    def __init__(self, sign, element):
        self.sign = sign
        self.element = element
    def __str__(self):
        return self.sign + str(self.element)

def Insert(x):
    return Edition('+', x)

def Delete(x):
    return Edition('-', x)

def plus(a, b):
    if a is None:
        return b
    elif b is None:
        return a
    else:
        return a + b

class Diff:
    def __init__(self, cost, changes):
        self.cost = cost
        self.changes = changes
    def __str__(self):
        return "{diff}({cost})".format(
                diff=list(map(str, self.changes)),
                cost=str(self.cost)
                )
    def __add__(self, other):
        return Diff(plus(self.cost, other.cost), self.changes + other.changes)
    def __lt__(self, other):
        return other.cost is not None and (self.cost is None or self.cost < other.cost)
    def __le__(self, other):
        return self.cost is None or (other.cost is not None and self.cost <= other.cost)
    def __gt__(self, other):
        return self.cost is not None and (other.cost is None or self.cost > other.cost)
    def __ge__(self, other):
        return other.cost is None or (self.cost is not None and self.cost >= other.cost)

Diff.empty = Diff(None, [])

class GLevenshtein:
    def __init__(self, cost):
        self.cost = cost
    def rate(self, operation):
        cost = self.cost(operation)
        return Diff.empty if cost is None else Diff(cost, [operation])
    def diff(self, l1, l2):
        return self._diff(l1, 0, l2, 0)
    def _diff(self, l1, i1, l2, i2):
        if i1 >= len(l1):
            if len(l2) == i2:
                return self.rate(None)
            else:
                return reduce(plus, map(self.rate, map(Insert, l2[i2:])))
        elif i2 >= len(l2):
            if len(l1) == i1:
                return self.rate(None)
            else:
                return reduce(plus, map(self.rate, map(Delete, l1[i1:])))
        else:
            if l1[i1] == l2[i2]:
                delta = Diff.empty
            else:
                delta = self.rate(Update(l1[i1], l2[i2]))
            return min(
                    (self.rate(Delete(l1[i1])) + self._diff(l1, i1+1, l2, i2)),
                    (self.rate(Insert(l2[i2])) + self._diff(l1, i1, l2, i2+1)),
                    (delta + self._diff(l1, i1+1, l2, i2+1))
                    )
