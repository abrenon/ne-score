from ne_score.cartesianCost import CartesianCost
from ne_score.generalLevenshtein import Diff, Edition, Update, GLevenshtein
import ne_score.tokens as tokens

class Cost(CartesianCost):
    def ranking():
        return ['missingOrExtra', 'wrongType', 'missingType', 'tokensDiff']

class NE:
    def __init__(self, NEType, wElems):
        self.NEType = NEType
        self.tokens = tokens.get([w.text for w in wElems])
    def __str__(self):
        return "|{tokens}|({NEType})".format(
                tokens=','.join(self.tokens),
                NEType=self.NEType
            )
    def __eq__(self, other):
        return self.NEType == other.NEType and self.tokens == other.tokens

def cost(operation):
    if type(operation) == Update:
        initial = operation.initial
        final = operation.final
        if len(set(initial.tokens).intersection(final.tokens)) > 0:
            missingType = final.NEType is not None and initial.NEType is None
            return Cost(
                    wrongType=not missingType and initial.NEType != final.NEType,
                    missingType=missingType,
                    tokensDiff=tokens.levenshtein.diff(
                        initial.tokens,
                        final.tokens
                        )
                    )
        else:
            return Cost(infinite=True)
    elif type(operation) == Edition:
        return Cost(missingOrExtra=1, tokensDiff=Diff.empty)
    else:
        return None

levenshtein = GLevenshtein(cost)

"""
    def diff(self, other):
        missingType = self.eNEType is None or other.eNEType is None
        return {
                'missingType': missingType,
                'wrongType': not missingType and self.eNEType != other.eNEType
                # TODO: write a levenshtein for  tokens with namesWeight and
                # otherWeight
            }
class Cost:
    def __init__(self, impossible, wrongType, missingType, tokensCost):
        self.impossible = impossible
        self.wrongType = wrongType
        self.missingType = missingType
        self.tokenCosts = tokenCosts
    def __add__(self, other):
        return Cost(
                self.impossible | other.impossible,
                self.name + other.name,
                self.minorWord + self.minorWord
                )
    def __lt__(self, other):
        selfTriple = (self.impossible, self.name, self.minorWord)
        otherTriple = (other.impossible, other.name, other.minorWord)
        return selfTriple < otherTriple
    def __le__(self, other):
        selfTriple = (self.impossible, self.name, self.minorWord)
        otherTriple = (other.impossible, other.name, other.minorWord)
        return selfTriple <= otherTriple
    def __gt__(self, other):
        selfTriple = (self.impossible, self.name, self.minorWord)
        otherTriple = (other.impossible, other.name, other.minorWord)
        return selfTriple > otherTriple
    def __ge__(self, other):
        selfTriple = (self.impossible, self.name, self.minorWord)
        otherTriple = (other.impossible, other.name, other.minorWord)
        return selfTriple >= otherTriple
"""

