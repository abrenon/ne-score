class CartesianCost:
    def __init__(self, **kwargs):
        if 'infinite' in kwargs:
            self.infinite = True
        else:
            self.infinite = False
            self.factors = {}
            for k in self.__class__.ranking():
                self.factors[k] = kwargs[k] if k in kwargs else 0
    def ranking():
        raise NotImplementedError("Must override ranking")
    def getCosts(self):
        if self.infinite:
            return (True,)
        else:
            factors = [self.factors[k] for k in self.__class__.ranking()]
            return (False, tuple(factors))
    def __add__(self, other):
        if self.infinite or other.infinite:
            return self.__class__(infinite=True)
        else:
            d = dict([
                (k, self.factors[k] + other.factors[k])
                for k in self.__class__.ranking()
                ])
            return self.__class__(**d)
    def __lt__(self, other):
        return self.getCosts() < other.getCosts()
    def __le__(self, other):
        return self.getCosts() <= other.getCosts()
    def __gt__(self, other):
        return self.getCosts() > other.getCosts()
    def __ge__(self, other):
        return self.getCosts() >= other.getCosts()
    def __str__(self):
        if self.infinite:
            return "infinite"
        else:
            return str(dict([(k, str(self.factors[k])) for k in self.factors]))
