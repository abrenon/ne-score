from distutils.core import setup

setup(
        name="NE-score",
        version="0.1.0",
        description="yeah, maybe",
        author="Alice Brenon",
        author_email="alice.brenon@liris.cnrs.fr",
        url="https://gitlab.liris.cnrs.fr/abrenon/ne-score",
        packages=["ne_score"],
        install_requires=['lxml'],
        scripts=['ne-score'],
        )

