(use-modules (gnu packages xml)
			 (guix build-system python)
			 (guix download)
			 (guix licenses)
			 (guix packages))

(package
  (name "ne-score")
  (version "0.1.0")
  (source
	(origin
	  (method url-fetch)
	  (uri ".")
	  (file-name (string-append name "-" version))
	  (sha256
		(base32 "0000000000000000000000000000000000000000000000000000"))))
  (build-system python-build-system)
  (propagated-inputs
	`(("python-lxml" ,python-lxml)))
  (home-page "https://gitlab.liris.cnrs.fr/abrenon/ne-score")
  (synopsis "A python tool to rate a named-entity annotator.")
  (description
	"This tool detects the named-entities in XML-TEI files and computes a diff
	on them to be able to measure the precision and recall of a named-entities
	annotator.

	The diff is computed by a generalization of the Levenshtein distance on
	lists expecting a cost function to describe the edition process and which
	can return an arbitrary (comparable) type. The implementation returns both
	the cost and the edition path.")
  (license bsd-3))
