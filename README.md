# NE score

Rate a named-entity annotator against a truth reference.

The `ne-score` script expects two arguments in this order:
- the path to the XML-TEI file containing the predictions
- the path to the XML-TEI file containing the truth

For now it simply displays the named-entity found in the prediction and truth,
then the diff computed on both.

TODO: convert this information in terms of precision/recall, and break it down
according to each named-entity type (Person, Date or Place).
