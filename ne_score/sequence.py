from lxml import etree
from ne_score.namedEntity import NE
import ne_score.namedEntity as nE
import re

def nEFromPerdidoElement(e):
    ws = e.xpath('.//w')
    wsType = None
    for w in ws:
        if 'type' in w.attrib:
            wsType = w.attrib['type']
    #if (e.attrib['type'], wsType) == ('place', 'NPr'):
    if e.attrib['type'] == 'place':
        return NE('Spatial', ws)
    elif e.attrib['type'] == 'date':
        return NE('Date', ws)
    elif e.attrib['type'] == 'person':
        return NE('Person', ws)
    else:
        return NE(None, ws)

posPrefix = re.compile("N._")

def nEFromTEIWAElement(e):
    if e.tag == 'w':
        words = [e]
    else:
        words = e.xpath('./w')
    return NE(posPrefix.sub("", e.attrib['enc_tags']), words)

def fromPerdido(dom):
    return map(nEFromPerdidoElement, dom.xpath('//rs[@subtype="no"]'))

def fromTEIWA(dom):
    elements = dom.xpath('//*[contains(@enc_tags, "NP_")]')
    return map(nEFromTEIWAElement, elements)

def nESequence(filePath):
    try:
        with open(filePath) as f:
            try:
                dom = etree.parse(f)
            except etree.XMLSyntaxError:
                return None
            rootTag = dom.getroot().tag
            sequencer = fromPerdido if rootTag == 'tei.2' else fromTEIWA
            return list(sequencer(dom))
    except FileNotFoundError:
        return None
