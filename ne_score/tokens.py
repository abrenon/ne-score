from ne_score.generalLevenshtein import Edition, Update, GLevenshtein
from ne_score.cartesianCost import CartesianCost
import re

tokenizer = re.compile("[- ']")

def get(words):
   return [s for word in words for s in tokenizer.split(word.strip()) if len(s) > 0] 

determiners = {
        "un", "une", "des"
        "le", "la", "l'", "les",
        }

prepositions = {
        "à", "de", "dans", "pour", "vers", "près", "en", "lès", "sur", "sous",
        "au", "aux", "du",
        }

smallWords = determiners.union(prepositions)

class Cost(CartesianCost):
    def ranking():
        return ['names', 'smallWords']

def cost(operation):
    if type(operation) == Update:
        return Cost(infinite=True)
    elif type(operation) == Edition:
        if operation.element in smallWords:
            return Cost(otherWords=1)
        else:
            return Cost(names=1)
    else:
        return None

levenshtein = GLevenshtein(cost)
